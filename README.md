Webpack 2
=========
##Tree Shaking

###This is a sample application to demonstrate the webpack tree shaking.


To enable tree-shaking with webpack 2 we need to exclude ***babel-plugin-transform-es2015-modules-commonjs*** plugin from ***babel-preset-es2015***.

That is possible only if we are including all the plugins from ***babel-preset-es2015*** individually except ***babel-plugin-transform-es2015-modules-commonjs***.

See the 'webpack.config.prod.js' for details.


In this project the dev code does not implement tree-shaking to speedup dev build time.

Tree-shaking is there only in prod build.

#### How

There is are two function inside helper.js, foo() and bar().

Only foo is imported and used in ***index.js***. So, if tree-shaking is happening successfully, the ***function bar()*** should not be included in production build.

```
#!javascript

"use strict";
    function r() {
        return "foo"
    }
```


Both the functions will be available in dev build.
```
#!javascript
    "use strict";
    /* harmony export */
    exports["a"] = foo; /* unused harmony export bar */
    function foo() {
        return 'foo';
    }
    function bar() {
        return 'bar';
    }

```


####How to run

- Clone the repo

```
git clone git@bitbucket.org:jyotirmaybanerjee/treeshaking.git && cd treeshaking
```

- Start dev server

```
npm start
```

- Build with source map
```
npm run build
```

- Build without source map
```
npm run build:prod
```
