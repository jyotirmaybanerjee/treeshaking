import React, { Component } from 'react';
import { render } from 'react-dom';
import { foo } from './helpers/helpers';

class App extends Component {

  render() {
	foo();
	return (
	  <h1>Hi</h1>
	);
  }
}

render(
  <App/ >,
  document.getElementById('root')
);
